import history from './history';

const createVirtualModule = ({ config, navigations, features }) => ({
    getConfig: () => config,
    getConfigValue: (pkg: string) => config[pkg],
    getNavigations: () => navigations,
    getNavigationsValue: (pkg: string) => navigations[pkg],
    getAllFeatures: () => features,
    getFeatures: (pkg: string) => features[pkg],
    getHistory: () => history,
})

export const defineVirtualModule = (params) => {
    const virtualModule = createVirtualModule(params)
    // @ts-ignore
    System.set('root.scope', {
        ...virtualModule
    });
}

