import "systemjs/dist/system";
import "systemjs/dist/extras/amd";
import "systemjs/dist/extras/named-register";
import "systemjs/dist/extras/named-exports";
import "systemjs/dist/extras/transform";
import "regenerator-runtime/runtime";

import { Apps } from './apps'
import { defineVirtualModule } from './virtual-module'
import history from './history';

const systemJSImport = async (requestUrl: string) => {
  const { default: component, mount, unmount } = await System.import(
    requestUrl
  )
  return { component, mount, unmount }
}

export default async ({ apps: rawApps, navigations, config, features }) => {
  defineVirtualModule({ navigations, config, features })

  const apps = new Apps(rawApps)

  let prevPathname = window.location.pathname

  async function getApp() {
    const app = apps.findApp(history.location.pathname)

    const publicPath = `/${app.name}/${app.version}`
  
    __webpack_public_path__ = `${config.baseUrl}${publicPath}${__webpack_public_path__}`
  
    const appPath = `${config.baseUrl}${publicPath}/index.js`
  
    const { component, mount, unmount } = await systemJSImport(appPath);

    return { component, mount, unmount };
  }

  let { component, mount, unmount } = await getApp()
  mount(component.default);

  history.listen(async (location) => {
    if (location.pathname !== prevPathname) {
      prevPathname = location.pathname
      unmount()

      const nextApp = await getApp();
      nextApp.mount(nextApp.component.default)
      unmount = nextApp.unmount;
    }
  })
};
