const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const outputDirectory = 'dist';

const pkg = require('./package.json')

module.exports = {
  mode: "development",
  entry: {
    index: "./src/main.ts"
  },
  output: {
    library: "fireapp",
    path: path.resolve(__dirname, outputDirectory),
    libraryExport: "default",
    publicPath: `/fireapp/${pkg.version}/`
  },

  plugins: [new CleanWebpackPlugin()],

  resolve: {
    extensions: [".ts", ".js"]
  },

  module: {
    rules: [
      { parser: { system: false } },
      {
        test: /\.tsx?$/,
        loader: "awesome-typescript-loader"
      },
      {
        test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/,
        loader: "file-loader"
      }
    ]
  }
};
